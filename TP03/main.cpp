#include "VectorD.hpp"
#include <iostream>
#include <fstream> 
#include <string>

int main(){

    VectorD vec1(3);

    vec1[1] = 5;
    vec1[2] = 42;

    std::string line = "save.txt";
    vec1.save(line);
    vec1.load(line);

    return 0;
}

/** NOTICE 
 * compilation :  g++ -Wall -O2 -std=c++11 main.cpp VectorD.cpp  -o main.out 
 * execution : ./main.out
 */
