#include "VectorD.hpp"
#include"math.h"

VectorD::VectorD(const size_t size, const double val):_data(size,val){

}

VectorD::VectorD(const VectorD &vec):_data(vec._data){

}


void VectorD::display() const{
    for(size_t i=0; i<_data.size();i++){
        std::cout << _data[i] << " ";

    }
    std::cout << std::endl;

}

double VectorD::dot(const VectorD &v) const{
    double produit=0;
    if(v._data.size()==_data.size()){
        for(size_t i=0; i<_data.size(); i++){
            produit+=_data[i]*v._data[i];
            
        }
        
    }else
        std::cout << "ALED les vecteurs n'ont pas la même taille" << std::endl;
    
    return produit;
}

double VectorD::norm()const {
    double norme=0;
    for(size_t i=0; i<_data.size();i++){
        norme+=_data[i]*_data[i];
        norme=sqrt(norme);
    }
    return norme;
}

const double& VectorD::operator[](const size_t& i) const{ 
    return _data[i];
}

double& VectorD::operator[](const size_t& i){
    return _data[i];
}

VectorD VectorD::operator+(const VectorD &v)const{
    VectorD v3(this->size());
    for(size_t i=0;i<this->size();i++){
        v3[i]= v[i] + _data[i];
    }
    return v3;
}

VectorD VectorD::operator-(const VectorD &v)const{
    VectorD v3(this->size());
    for(size_t i=0;i<this->size();i++){
        v3[i]= _data[i] - v[i];
    }
    return v3;
}

VectorD VectorD::operator*(const double &d)const{
    VectorD v3(this->size());
    for(size_t i=0;i<this->size();i++){
        v3[i]= _data[i] * d;
    }
    return v3;
}

VectorD VectorD::operator-()const{
    VectorD v3(this->size());
     for(size_t i=0;i<this->size();i++){
        v3[i]= -_data[i];
    }
    return v3;
}


std::ostream& operator<< (std::ostream& stream, const VectorD& v){
    
    stream << "[ ";
    for (size_t i=0; i<v.size()-1;i++){
        stream << v[i] << ", ";
    }
    stream << v[v.size()-1] << " ]";

    return stream;

}

void VectorD::save (const std::string &filename) const {
    std::ofstream fichier (filename);
    fichier <<this->size() <<std::endl;
    for (size_t i = 0; i<this->size(); i++){
        fichier << _data[i] <<std::endl;
    }
    
}

void VectorD::load (const std::string &filename) const {
    std::ifstream fichier (filename, std::ios::in);

    if (fichier) {
        size_t taille;
        fichier >> taille;
        std::cout << "taille lue : " << taille << std::endl;

        VectorD v(taille);
        for (size_t i = 0; i < taille; i++){
            fichier >> v[i];
        }
        std::cout << v << std::endl;

        fichier.close();
    } else 
        std::cerr << "Impossible d'ouvrir le fichier !"<<std::endl;    
}
