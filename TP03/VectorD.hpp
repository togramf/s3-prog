#ifndef  VECTOR_D_TP2
#define  VECTOR_D_TP2
#include <vector>
#include <iostream>
#include <fstream> 

class VectorD{

private:

    std::vector<double> _data;

public: 

    VectorD(const size_t size=0, const double val=0);
    VectorD(const VectorD &vec);
    ~VectorD()=default;
    
    inline const size_t  size() const;
    double dot(const VectorD &v)const;
    double norm()const;

    void display()const;

    //Opérateurs
    const double& operator[] (const size_t& i) const;
    double& operator[] (const size_t& i);
    VectorD operator+ (const VectorD &v) const;
    VectorD operator- (const VectorD &v) const;
    VectorD operator* (const double &d) const;
    VectorD operator-()const;

    void save (const std::string &filename) const;  
    void load (const std::string &filename) const;  

};

inline const size_t  VectorD::size() const{
    return _data.size();
}

std::ostream& operator<< (std::ostream& stream, const VectorD& v);
inline VectorD operator* (const double &val, const VectorD &v){
    return v*val;
}


#endif