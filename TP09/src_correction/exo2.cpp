double pow1(const double &x, const unsigned int n)
{
  if(n==0) return 1.0;
  else return x * pow1(x,n-1);
}

template<typename T>
T pow2(const T &x, const unsigned int n)
{
  if(n==0) return static_cast<T>(1);
  else return x * pow2<T>(x,n-1);
}

template <typename T>
constexpr T pow3(const T &x, const unsigned int n){
  return (n==0) ? static_cast<T>(1) : x * pow3<T>(x, n-1);
}

std::cout << "pow1(5,3) = " << pow1(5,3) << std::endl;
std::cout << "pow2(5,3) = " << pow2(5,3) << std::endl;
std::cout << "pow3(5,3) = " << pow3(5,3) << std::endl;
static_assert(pow3(5,3) == 125, "test of ’pow3’ at compile time");