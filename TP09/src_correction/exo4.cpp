template<typename T>
T multVec1(const std::vector<T> &vec){
  T res = static_cast<T>(1);
  for(const auto &v : vec)
    res *= v;

  return res;
}

template<typename T>
T multVec2(const std::vector<T> &vec){
  T res = static_cast<T>(1);
  std::for_each(vec.begin(), vec.end(), [&res] (const T &v){res *= v;} );
  return res;
}

std::cout << "vector initialisation : ";
std::vector<int> vec1 = {1,2,3,4,5};
for(const auto & v : vec1)
    std::cout << v << " ";
std::cout << std::endl;

std::cout << "multVec1(vec1) = " << multVec1(vec1) << std::endl;
std::cout << "multVec2(vec1) = " << multVec2(vec1) << std::endl;

//correction 2 
template<typename T>
T multVec3(const std::vector<T> &vec){
  return std::accumulate(vec.begin(), vec.end(), static_cast<T>(1), std::multiplies<T>());
}

template<typename T>
std::vector<T> generateVector(const unsigned int size){
  std::vector<T> vec(size);
  T value = static_cast<T>(1);
  std::generate(vec.begin(), vec.end(), [&value] { return value++; });
  return vec;
}


std::cout << "multVec1(vec1) = " << multVec1(vec1) << std::endl;
std::cout << "multVec2(vec1) = " << multVec2(vec1) << std::endl;
std::cout << "multVec3(vec1) = " << multVec3(vec1) << std::endl;

std::vector<int> vec2 = generateVector<int>(20);
std::cout << "generate a bigger vector with tab[i]=i  : ";
for(const auto & v : vec2)
    std::cout << v << " ";
std::cout << std::endl;
