//bouts de correction en vrac
// A RELIRE

template<typename T>
constexpr
T my_abs(const T x)
noexcept
{
  return x < static_cast<T>(0) ? -x : x;
}

std::cout << "my_abs(-6)   = " << my_abs(-6) << std::endl;
std::cout << "my_abs(-6.5) = " << my_abs(-6.5) << std::endl;
std::cout << "my_abs(-0)   = " << my_abs(-0) << std::endl;
std::cout << "my_abs(42.0) = " << my_abs(42.0) << std::endl;
static_assert(my_abs(-5) == 5, "test of ’my_abs’ at compile time");