template<typename T>
constexpr
T product(const T &v) {
  return v;
}

template<typename T, typename... Args>
constexpr
T product(const T &first, Args... args) {
  return first * product(args...);
}

std::cout << "product(5,5,5)     = " << product(5,5,5) << std::endl;
std::cout << "product(1,2,3,4,5) = " << product(1,2,3,4,5) << std::endl;
static_assert(product(5,5,5) == 125, "test of 'product' at compile time");
constexpr auto res = product<double>(1,2.333333f,3.5f,4,5);
std::cout << "product(1,2.333333f,3.5f,4,5) = " << res 
        << "  -- with type : " << typeid(res).name() << std::endl;