// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 4
//  - découvrir les vecteur de la STL 
// ================================

#include<iostream>
#include<vector>

namespace TP_CPP_IMAC2
{
	int main(int argc, char *argv[])
	{
		std::vector<int> vec;
        int taille;
        std::cin >> taille;

        for(int i=0;i<taille;i++){
            vec.push_back(i%10);
        }

        std::cout << "taille du vecteur : " <<  vec.size() << std::endl;
        
        for(int i=0;i<taille;i++){
            std::cout << "l'élément " << i << " vaut " << vec[i] << std::endl;
        }

        for(int i=0;i<3;i++){
            std::cout << "adresse de l'élément " << i << " est " << &vec[i] << 
            " et sa taille vaut " << sizeof(vec[i]) <<std::endl;
        }

        std::cout << "taille du vecteur : " << vec.size() << std::endl;
        vec.pop_back();
        std::cout << "taille du vecteur : " << vec.size() << std::endl;
        

        for(int i=0;i<vec.size();i++){
            std::cout << "l'élément " << i << " vaut " << vec[i] << std::endl;
        }

        vec.clear();
        std::cout << "taille du vecteur : " << vec.size() << std::endl;
        
        
        return 0;

	}
}

// Fonction main classique, point d'entrée du programme
int main(int argc, char *argv[])
{
	// Appel de la fonction main de l'espace de nom TP_CPP_IMAC2
	return TP_CPP_IMAC2::main(argc, argv);
}


