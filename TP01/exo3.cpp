// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 3
//  - Manipulation de string 
// ================================

#include<iostream>
#include<string>

int main(){
    std::string chaine;
    std::cin >> chaine;
    std::cout << "String size : " << chaine.size()<< std::endl << "Last element : " << chaine.back() << std::endl;
    chaine.pop_back();
    chaine="IMAC"+chaine;
    std::cout << chaine <<std::endl;
}

/** NOTICE 
 * compilation :  g++ -Wall -O2 -std=c++11 exo3.cpp -o exo3.out
 * execution : ./exo3.out 
 */
