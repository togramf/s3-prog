// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 2
// ================================

#include<iostream>

int main(){
    int var;
    std::cin >> var;
    if(var==42)
        std::cout << "parfait" <<std::endl;
    else if(var<0)
        std::cout << "négatif" <<std::endl;

    if(var>0)    
        std::cerr << "Strictement positif" << std::endl;

}

/** NOTICE 
 * compilation :  g++ -Wall -O2 exo2.cpp -o exo2.out
 * execution : ./exo2.out 
 */
