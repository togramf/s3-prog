// ================================
// POO C++ - IMAC 2
// TP 1 - Exercice 5
// ================================

#include<iostream>
#include<vector>
#ifndef __CHRONO_HPP__
#define __CHRONO_HPP__

#include <chrono>

namespace TP_CPP_IMAC2
{
    class Chrono
	{
	public:
		void start() 
		{ 
			begin = std::chrono::high_resolution_clock::now(); 
		}
		void stop() 
		{ 
			end = std::chrono::high_resolution_clock::now(); 
		}
		double timeSpan() const
		{ 
			return (std::chrono::duration_cast<std::chrono::duration<double>>(end - begin)).count();  
		}
	
	private:
		std::chrono::high_resolution_clock::time_point begin;
		std::chrono::high_resolution_clock::time_point end;
	};

    float mean(std::vector<int> &vec){
    
        float sum=0;
        for(unsigned int i=0; i<vec.size(); i++){
            sum=sum+vec[i];
        }
        return sum/vec.size();
    }

    float meanCopy(std::vector<int> vec){
    
        float sum=0;
        for(unsigned int i=0; i<vec.size(); i++){
            sum=sum+vec[i];
        }
        return sum/vec.size();
    }

	int main(int argc, char *argv[])
	{
		std::vector<int> vec;
        int taille;
        std::cin >> taille;

        for(int i=0;i<taille;i++){
            vec.push_back(i%10);
        }

        std::cout << "La moyenne du vecteur vaut " << TP_CPP_IMAC2::mean(vec) << std::endl;
        
        TP_CPP_IMAC2::Chrono chrono;
        chrono.start();
        TP_CPP_IMAC2::mean(vec);
        chrono.stop();
        std::cout << "Temps mean: " << chrono.timeSpan() << " s " << std::endl;

        TP_CPP_IMAC2::Chrono chrono2;
        chrono2.start();
        TP_CPP_IMAC2::meanCopy(vec);
        chrono2.stop();
        std::cout << "Temps meanCopy: " << chrono2.timeSpan() << " s " << std::endl;

        return 0;

	}




}

// Fonction main classique, point d'entrée du programme
int main(int argc, char *argv[])
{
	// Appel de la fonction main de l'espace de nom TP_CPP_IMAC2
	return TP_CPP_IMAC2::main(argc, argv);
}


#endif // __CHRONO_HPP__