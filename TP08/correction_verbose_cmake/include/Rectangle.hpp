#pragma once

#include "Shape.hpp"

class Rectangle : public Shape {

protected:

	double m_width  = 0; 
	double m_height = 0;

public:

	Rectangle(); 
	Rectangle(const double width, const double height);
	~Rectangle(); 

	inline void setWidth(const double & width) { m_width = width; }
	inline const double getWidth() const { return m_width; }

	inline void setHeight(const double & height)  { m_height = height; }
    inline const double getHeight() const { return m_height; }

	double surface() const override; 
    void draw() const override;
};

