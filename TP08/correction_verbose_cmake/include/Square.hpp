
#pragma once

#include "Shape.hpp"
#include "Rectangle.hpp"


class Square : public Rectangle {

public :

	Square();
	Square(const double c);
	~Square();

	void setWidth(const double & width)   = delete;
	void setHeight(const double & height) = delete;
  	inline double getSide() const { return m_width;}
  	inline void setSide(const double & side) { m_width = m_height = side;}
  	
    void draw() const override;
};

