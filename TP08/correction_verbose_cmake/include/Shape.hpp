#pragma once

constexpr bool verbatim = false;

class Shape{

public :
	
	virtual ~Shape(){};

	virtual void draw() const = 0;

	virtual double surface() const = 0;

};
