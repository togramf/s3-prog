#include <iostream>
#include "Circle.hpp"


Circle::Circle() : m_radius(0)
 {
 	if constexpr (verbatim) 
 		std::cout << "Default constuctor Circle" << std::endl;
 }


Circle::Circle(const double radius) : m_radius(radius)
 {
 	if constexpr (verbatim) 
 		std::cout << "Constructor Circle" << std::endl;
 }


Circle::~Circle(){
 	if constexpr (verbatim) 
 		std::cout << "Destructor Circle" << std::endl;	
}


double Circle::surface() const{
	return 3.14 * m_radius * m_radius;
}


void Circle::draw() const{
	std::cout << "  -- Circle    (" << surface() << ")" << std::endl;
}