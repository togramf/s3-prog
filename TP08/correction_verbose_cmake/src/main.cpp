#include <iostream>
#include <vector>
#include <functional> // std::reference_wrapper

#include "Shape.hpp"
#include "Rectangle.hpp"
#include "Circle.hpp"
#include "Square.hpp"


// g++ -Wall -O2 -std=c++17  main.cpp Rectangle.cpp Circle.cpp Square.cpp -o shape


void plop(const Shape &shape){
	shape.draw();
}


int main(){


    {
		Rectangle r(10,20);
		r.setHeight(10);
		plop(r);

		Circle c(2);
		plop(c);

		Square s(4);
		// s.setHeight(20); // forbiden
        s.setSide(10);
        std::cout << s.getHeight() << std::endl;
		plop(s);

    } // to force the destructor



    {	
    	std::vector<Shape*> shapes;

    	for(size_t i=0; i<3; ++i)
    		shapes.push_back(new Rectangle(i,i));
    	
    	for(size_t i=0; i<3; ++i)
    		shapes.push_back(new Circle(i));
    	
    	for(size_t i=0; i<3; ++i)
    		shapes.push_back(new Square(i));

    	for(size_t i=0; i<shapes.size(); ++i)
    		plop(*(shapes[i]));
    	
    	for(size_t i=0; i<shapes.size(); ++i)
    		delete shapes[i];
    }
    

	return 0;
}