#include <iostream>
#include "Rectangle.hpp"


Rectangle::Rectangle() : m_width(0), m_height(0)
 {
 	if constexpr (verbatim) 
 		std::cout << "Default constuctor Rectangle" << std::endl;
 }


Rectangle::Rectangle(const double width, const double height) : m_width(width), m_height(height)
 {
 	if constexpr (verbatim) 
 		std::cout << "Constructor Rectangle" << std::endl;
 }


Rectangle::~Rectangle(){
 	if constexpr (verbatim) 
 		std::cout << "Destructor Rectangle" << std::endl;	
}


double Rectangle::surface() const{
	return m_height * m_width;
}


void Rectangle::draw() const{
	std::cout << "  -- Rectangle (" << surface() << ")" << std::endl;
}