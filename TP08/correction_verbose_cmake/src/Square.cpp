#include <iostream>
#include "Square.hpp" 


Square::Square() : Rectangle()
{
 	if constexpr (verbatim) 
 		std::cout << "Default constuctor Square" << std::endl;
}


Square::Square(const double c)
 : Rectangle(c,c)
{
 	if constexpr (verbatim) 
 		std::cout << "Constuctor Square" << std::endl;
}


Square::~Square(){
 	if constexpr (verbatim) 
 		std::cout << "Destuctor Square" << std::endl;	
}


void Square::draw() const{
	std::cout << "  -- Square    (" << surface() << ")" << std::endl;
}