#include <iostream>
#include <vector>

#include "../include/Shape.hpp"
#include "../include/Rectangle.hpp"
#include "../include/Circle.hpp"
#include "../include/Square.hpp"
#include "../include/Isocele.hpp"


// g++ -Wall -O2 -std=c++11  main.cpp Rectangle.cpp Circle.cpp Square.cpp Isocele.cpp -o shape


void plop(const Shape &shape){
	shape.draw();
}


int main(){
	//test classe Circle
	Circle c(2);
	c.draw();

	//test classe Rectangle
	Rectangle r(10,20);
	r.setHeight(5);
	r.draw();

	// test classe Square
	Square s(60);
	s.draw();
	//s.setWidth();

	//test classe Isocele
	Isocele i(3.,5.);
	i.draw();


	//Vecteur de shapes	
	std::vector<Shape *> shapes;
		//remplissage de shapes
	for(size_t j = 0; j<3; j++){
		shapes.push_back(new Rectangle(j,j));
	}
	shapes.push_back(new Square(4));

		//affichage des shapes 
	for (const auto shape : shapes){
		plop(*shape);
	}

	for (const auto shape : shapes){
		delete shape;
	}
	

	return 0;
}