#include <iostream>
#include "../include/Isocele.hpp" 
#include <cmath>

Isocele::Isocele(const double &base, const double &side) 
    : m_base(base), m_side(side){}

double Isocele::surface() const {
    double hauteur = 0;
    if (m_base/2 > m_side)
        hauteur = sqrt(m_side*m_side - (m_base/2)*(m_base/2));
    return m_base * hauteur;
}

void Isocele::draw() const {
    std::cout << " -- triangle isocele " << std::endl;
}