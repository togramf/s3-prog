#include <iostream>
#include "../include/Rectangle.hpp"

Rectangle::Rectangle(const double &width, const double &height) 
    : m_width(width), m_height(height){}

double Rectangle::surface() const{
    return m_height*m_width;
} 

void Rectangle::draw() const{
    std::cout << " -- rectangle " << std::endl;
}