#include <iostream>
#include "../include/Circle.hpp"
#include <cmath>


Circle::Circle(){
    m_radius=0;
}

Circle::Circle(const double radius) : m_radius(radius){
}

double Circle::surface() const{
    return M_PI*m_radius*m_radius;
} 

void Circle::draw() const{
    std::cout << " -- circle " << std::endl;

}

