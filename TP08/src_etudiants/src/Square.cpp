#include <iostream>
#include "../include/Square.hpp" 

Square::Square(const double &side) : Rectangle::Rectangle (side,side){}

void Square::draw() const{
    std::cout << " -- square " << std::endl;
}

