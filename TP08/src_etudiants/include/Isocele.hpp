#pragma once

#include "Shape.hpp"

class Isocele : public Shape {

protected : 
    double m_base;
    double m_side; 

public :

	Isocele();
	Isocele(const double &base,const double &side);
	~Isocele() = default;

	inline void setBase(const double & base){ m_base = base;}
	inline void setSide(const double & side){ m_side = side;}

  	inline double getBase() const { return m_base;}
  	inline double getSide() const { return m_side;}
  	
    double surface() const override;
    void draw() const override;
};