cmake_minimum_required(VERSION 3.13)


# give a name to the project
project(my_map LANGUAGES C CXX)

# files to compile
add_executable(my_map src/main.cpp)

# compilation flags
target_compile_features(my_map PRIVATE cxx_std_11)
target_compile_options(my_map PRIVATE -Wall)

