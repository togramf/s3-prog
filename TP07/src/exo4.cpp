#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <numeric>
#include <list>
#include <string>


void afficheVector (const std::vector<int> &vec){
    for (const auto &it:vec)
        std::cout << it << " " ;
    std::cout << std::endl;
}

void afficheList (const std::list<std::string> &list){
    for (const auto &it:list)
        std::cout << it << " " ;
    std::cout << std::endl;
}


int main(){

    std::list<std::string> philo;
    philo.push_back("Platon");
    philo.push_back("Aristote");
    philo.push_back("Descartes");
    philo.push_back("Kant");

    std::list<std::string> math;
    math.push_back("Gauss");
    math.push_back("Laplace");
    math.push_back("Poincaré");
    math.push_back("Descartes");

    philo.sort();
    math.sort();

    afficheList(philo);
    afficheList(math);

    std::list<std::string> philo_math(8);
    std::merge(philo.begin(),philo.end(),math.begin(),math.end(),philo_math.begin());

    afficheList(philo_math);

    std::list<std::string> all(8);

    std::unique(philo_math.begin(), philo_math.end());
    afficheList(philo_math);

    std::reverse(philo_math.begin(), philo_math.end());
    afficheList(philo_math);
    
    return 0;
}