#include<iostream>
#include<vector>
#include <functional>


void afficheVector (const std::vector<int> &vec){
    for (const auto &it:vec)
        std::cout << it << " " ;
    std::cout << std::endl;
}


int main(){

    std::vector<int> vec;
  
    for (uint i=0; i<10; i++){
        vec.push_back(i);
     
    }

    afficheVector(vec); 
    return 0;
}