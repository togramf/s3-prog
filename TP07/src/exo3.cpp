#include<iostream>
#include<vector>
#include <functional>
#include <algorithm>
#include <numeric>


void afficheVector (const std::vector<int> &vec){
    for (const auto &it:vec)
        std::cout << it << " " ;
    std::cout << std::endl;
}

bool sort2(int a, int b) {
    if (a%2==0) {
        if(b%2!=0)
            return 1;
    } else {
        if(b%2==0)
            return 0;
    }
    return(a<b);
}

int main(){

    std::vector<int> vec;
  
    for (uint i=0; i<10; i++){
        vec.push_back(10-i);
    }

    std::vector<int> vec2;
  
    for (uint i=0; i<10; i++){
        vec2.push_back(20-2*i);
    }

    afficheVector(vec); 

    std::sort(vec.begin(), vec.end());
    afficheVector(vec);

    std::sort(vec.begin(), vec.end(),sort2);
    afficheVector(vec);

    std::cout << "nombre d'occurence de 7 : " << std::count(vec.begin(), vec.end(),7) << std::endl<< std::endl;

    afficheVector(vec2); 

    std::cout << std::inner_product(vec.begin(), vec.end(), vec2.begin(),10)<< std::endl;

    
    return 0;
}