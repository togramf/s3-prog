
#include<iostream>
#include<vector>
#include <functional>

int main(){

    std::vector<int> vec;

    for (uint i=0; i<10; i++){
        vec.push_back(i);
        std::cout << " taille du vecteur   : " << vec.size() << std::endl;
        std::cout << " capacité du vecteur : " << vec.capacity() << std::endl; 
        //vec.shrink_to_fit();
        //std::cout << " capacité du vecteur : " << vec.capacity() << std::endl; 
    }

    std::vector<int> vec2;
    vec2.reserve(10);
    for (uint i=0;i<20;i++){
        vec2.push_back(i);
        std::cout << " taille du vecteur 2    : " << vec2.size() << std::endl;
        std::cout << " capacité du vecteur 2  : " << vec2.capacity() << std::endl; 
    }

    std::vector<int> vec3(10);
    for (uint i=0;i<20;i++){
        vec3.push_back(i);
        std::cout << " taille du vecteur 3    : " << vec3.size() << std::endl;
        std::cout << " capacité du vecteur 3  : " << vec3.capacity() << std::endl; 
    }
    std::cout << vec3[6] << std::endl;

    return 0;
}