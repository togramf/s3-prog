#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <numeric>
#include <cmath>

#ifndef __VECTORD__HPP
#define __VECTORD__HPP

// g++ -Wall -O2 -std=c++11 main.cpp VectorD.hpp

template<typename T>
class VectorD {

public:

	// default constructor / size constructor / initializer constructor
    VectorD(const size_t size = 0, const T &value = static_cast<T>(0))
    	: _data(size,value)
    	{};

    // copy constructor
	VectorD(const VectorD & v) = default;

	// destructor
    ~VectorD() = default;

    // copy constructor from other type
#if 1
    template <typename U>
    friend class VectorD;

	template<typename U>
	VectorD<T>(const VectorD<U> &vec) 
		: VectorD<T>(vec.size())
	{
		// access to a private member of VectorD<U>, VectorD<T> and VectorD<U> must be friend
		std::transform(vec._data.begin(), vec._data.end(), _data.begin(), [](const U &x) { return static_cast<T>(x);});
	}
#else
	template<typename U>
	VectorD<T>(const VectorD<U> &vec) 
		: VectorD<T>(vec.size())
	{
		for(size_t i=0; i<vec.size(); ++i)
			_data[i] = static_cast<T>(vec[i]); 
	}
#endif


private :
  
  	// attribute (components of the vector)
    std::vector<T> _data;


public :
	
	// methods
	size_t size() const { return _data.size(); }
	T dot(const VectorD &v) const;
    T norm() const;
    void normalize();
    bool save(const std::string &filename) const;
    bool load(const std::string &filename);

    // operators
	VectorD & operator=(const VectorD &v);
	T& operator[](const size_t& i) {return _data[i];};
	const T& operator[](const size_t& i) const {return _data[i];};
	VectorD operator+(const VectorD &v) const;
	VectorD operator-(const VectorD &v) const;
	VectorD operator-() const;
	VectorD operator*(const T &value) const;
};



template<typename T>
VectorD<T> & VectorD<T>::operator=(const VectorD<T> &v){
	if(&v == this) return *this;

	_data = v._data;

	return *this;
}


template<typename T>
VectorD<T> VectorD<T>::operator+(const VectorD<T> &v) const
{
	assert(v.size() == this->size() && "error: VectorD::operator+: operands with incompatible size");

	VectorD<T> result(size());
	std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::plus<double>());

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator-(const VectorD<T> &v) const
{
	assert(v.size() == this->size() && "error: VectorD::operator-: operands with incompatible size");

	VectorD<T> result(size());
	std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::minus<double>());

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator-() const
{
	VectorD<T> result(size());
	for(size_t i=0; i<size(); ++i)
		result[i] = -_data[i];

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator*(const T &value) const
{
	VectorD<T> result(*this);
	for(size_t i=0; i<size(); ++i)
		result[i] *= value;

	return result;
}


template<typename T>
T VectorD<T>::dot(const VectorD<T> & v) const {  

	assert(v.size() == this->size() && "error: VectorD::dot: operands with incompatible size");

    // T result = static_cast<T>(0);
    // for(size_t i=0; i<size(); ++i)
    // 	result += _data[i]*v[i];
    // return result;

   return std::inner_product(_data.begin(), _data.end(), v._data.begin(), static_cast<T>(0));
}


template<typename T>
T VectorD<T>::norm() const{
	return sqrt(this->dot(*this));
}


template<typename T>
void VectorD<T>::normalize() {
    T vec_norm = this->norm();

    if (std::abs(vec_norm) < static_cast<T>(1.0e-10))
        return;
    
    for (size_t i =0; i<size(); i++) 
        _data[i] /= vec_norm;
}


template<typename T>
std::ostream& operator<< (std::ostream& stream, const VectorD<T>& v){
	if(v.size() == 0){
		stream << "Not initialized yet -> size is 0";
		return stream;
	}
			
	stream << "(";
	for(unsigned int i=0; i<v.size()-1; ++i)
		stream << v[i] << " , ";
 
 	stream << v[v.size()-1] << ")";

	return stream;
}


template<typename T>
bool VectorD<T>::save(const std::string &filename) const{

	//open the file
	std::ofstream myfile;
	myfile.open(filename, std::ios::out | std::ios::binary);
	
    if(!myfile.is_open()){
        std::cerr << "error: can not create file: " << filename << std::endl;
        return false;
    }

	// write the vector size
	myfile << size() << std::endl;

	for(size_t i=0; i<size(); ++i)
		myfile << _data[i] << " ";

	myfile.close();

	return true;
}


template<typename T>
bool VectorD<T>::load(const std::string &filename){

	//open the file
	std::ifstream myfile;
	myfile.open(filename, std::ios::in | std::ios::binary);
    if(!myfile.is_open()){
        std::cerr << "error: can not open file: " << filename << std::endl;
        return false;
    }

	// read the vector size
	size_t vectorSize;
	myfile >> vectorSize;
	if(vectorSize != size())
        *this = VectorD(vectorSize);

    // read the data
	for(size_t i=0; i<vectorSize; ++i)
		myfile >> _data[i];

	// close file
	myfile.close();
	return true;
}




#endif
