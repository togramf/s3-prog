#include <iostream>
#include <cassert>


#include "VectorD.hpp"


VectorD::VectorD(const size_t size, const double value) 
: _data(size,value) 
{}


VectorD& VectorD::operator=(const VectorD &vec)
{
    if(&vec == this) return *this;

    _data = vec._data;

    return *this;
}


void VectorD::display() const {
    for(size_t i=0; i< _data.size(); ++i)
        std::cout << _data[i] << " ";
    std::cout << std::endl;
}


double VectorD::dot(const VectorD & vec) const {
    assert(_data.size() == vec._data.size());

    double result = 0.0;
    for(size_t i=0; i<_data.size(); ++i)
        result += _data[i] * vec._data[i];

    return result;
}