#ifndef __VECTORD__HPP__
#define __VECTORD__HPP__

#include <iostream>
#include <vector>
#include <cmath>

class VectorD {

public:

    // constructor and default constructor
    VectorD(const size_t size=0, const double value = 0.0);

    // copy constructor
    VectorD(const VectorD &v) = default;

    // destructor
    ~VectorD() = default;

    // methods
    void display() const;
    double dot(const VectorD &v) const;
    inline double norm() const { return sqrt(this->dot(*this)); }

    // operators
    VectorD& operator=(const VectorD &vec);


public :
    
    std::vector<double> _data;
};

#endif