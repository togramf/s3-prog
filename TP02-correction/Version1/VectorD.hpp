#ifndef __VECTORD__HPP__
#define __VECTORD__HPP__

#include <iostream>
#include <vector>

class VectorD {

public:

    // constructor and default constructor
    VectorD(const size_t size=0, const double value = 0.0);

    // copy constructor
    VectorD(const VectorD &v);

    // destructor
    ~VectorD();

    // methods
    void   display() const;
    double dot(const VectorD &v) const;
    double norm() const;

    // operators
    VectorD& operator=(const VectorD &vec);


public :
    
    std::vector<double> _data;

};

#endif