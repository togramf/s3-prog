#include <iostream>
#include <cstdlib>

#include "./Version1/VectorD.hpp"
// #include "./Version2/VectorD.hpp"

// g++ -Wall -O2 -std=c++11 main.cpp VectorD.cpp -o vec.exe
// Fonctionne avec les deux versions de correction

int main(int argc, char **argv) {

    VectorD v1(3);
    v1._data[0] = 1.0;
    v1._data[1] = 0.5;
    v1._data[2] = 2.0;

    std::cout << "v1 = ";
    v1.display();

    VectorD v2(3,3.0);
    std::cout << "v2 = ";
    v2.display();

    std::cout << "v1.v2   = " <<  v1.dot(v2) << std::endl;
    std::cout << "v2.norm = " <<  v2.norm() << std::endl;

    v1 = v2;
    std::cout << "v1 = v2 -> v1 = ";
    v1.display();

    return 0;
}