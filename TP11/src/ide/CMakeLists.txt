cmake_minimum_required(VERSION 3.13)

# give a name to the project
project(myCmakeTest)

# (optaional) put the binary files in this directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)

# add myLib
message(STATUS "myLib cmake part ..." )
add_subdirectory(myLib INTERFACE)
include_directories(${CMAKE_SOURCE_DIR}/myLib/include) # important : other projects call by this cmake will know this include directory

# add myCode
message(STATUS "myCode cmake part ..." )
add_subdirectory(myCode)

# (optional) debug or release
#set(CMAKE_BUILD_TYPE Debug)
#set(CMAKE_BUILD_TYPE Release)

