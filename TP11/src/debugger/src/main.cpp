#include <iostream>
#include <string>
#include <cmath>



double plop_sum_inv(){
    // sum  1/(2n)
    const unsigned int nbIteration = 20;
    double result = 0;
    for(unsigned int i=1;i<nbIteration; ++i)
        result += (1/2) * (1/i);

    return result;
}

void plop_seg_fault()
{
    int *tab = nullptr; // int *tab = new int[number];

    const size_t index = 100000000;
    std::cout << "plop" << tab[index] << std::endl;

    delete[] tab;
}



constexpr unsigned int plop_factoriel(const unsigned int n){
    return (n == 0) ? 1 : n * plop_factoriel(n-1);
}


double plop_exp(const double &x)
{
    // exp(x) ~ sum (x^n)/n! (Taylor expension)
    const int nbIteration = 30;
    double xn = 1.0;
    double res = 0.0;
    for(int i=0; i<nbIteration; ++i){
        const unsigned int factoriel = plop_factoriel(i);
        res += xn / factoriel;
        xn *= x;
    }

    return res;
}


int main(int argc, char** argv) {

    std::cout << "sum 1/(2n) = " << plop_sum_inv() << std::endl;

    plop_seg_fault();

    // const double value = 2;
    // std::cout << "math : exp(" << value << ") = " << std::exp(value) << std::endl;
    // std::cout << "plop : exp(" << value << ") = " << plop_exp(value) << std::endl;

    return 0;
}

