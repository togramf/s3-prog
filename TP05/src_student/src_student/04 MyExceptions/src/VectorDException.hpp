#include <iostream>
#include <cstdlib>

class VectorDException : public std::exception
{
    public :
        exception() throw(){}
        virtual ~exception() throw();
        virtual const char* what() const throw();
}