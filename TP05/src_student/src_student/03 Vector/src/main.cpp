#include <iostream>
#include <stdexcept>

#include "VectorD.hpp"



int main(int argc, char **argv) {

    VectorD v1(3);
    v1[0] = 1.0;
    v1[1] = 0.3; 
    v1[2] = 5.2;

    VectorD v2(4);
    v2[0] = -1.0;
    v2[1] =  3.0; 
    v2[2] = 42.0;
    v2[3] =  2.0;

    try{
        std::cout << "v1 + v2  : " << v1 + v2 << std::endl;
    }catch(const std::length_error &s){
        std::cerr << "error: "<< s.what() << std::endl;
    }

    try{
        std::cout << "v1 - v2  : " << v1 - v2 << std::endl;
    }catch(const std::length_error &s){
        std::cerr << "error: "<< s.what()<< std::endl;
    }

    try {
        std::cout << "v1.v2    : " << v1.dot(v2) << std::endl;
    } catch (const std::length_error &s){
        std::cerr << "error: "<< s.what() << std::endl;
    }

    VectorD v3;
    try  {
        v3.load("wazoo.vec"); 
    } catch (const std::invalid_argument &ia){
        std::cerr <<"error: "<< ia.what() << std::endl;
    }
    

    return 0;
}
