#pragma once

#include <exception>
#include <string>

enum class ErrorLevel {minor, major, fatal};

std::string ErrorLevel2Sstring(const ErrorLevel &errorLevel);

class VectorDException : public std::exception
{
	public:

		VectorDException(const std::string &description = "", const int &errorId = 0, const ErrorLevel &errorLevel = ErrorLevel::minor) noexcept ;

		virtual ~VectorDException() noexcept {}

	public:
		const char *what() const noexcept override 
		{
			return m_what.c_str();
		}

	private:
		std::string m_what;
};





