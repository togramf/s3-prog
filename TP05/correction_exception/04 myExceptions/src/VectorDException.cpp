#include <string>

#include "VectorDException.hpp"


// convert enum to string
std::string ErrorLevel2Sstring(const ErrorLevel &errorLevel){
	std::string errorLevelString;
	switch(errorLevel){
		case ErrorLevel::minor :
			errorLevelString = "minor";
			break;
		case ErrorLevel::major :
			errorLevelString = "major";
			break;
		case ErrorLevel::fatal :
			errorLevelString = "fatal";
			break;
		default : 
			errorLevelString = "unknown";
	};

	return errorLevelString;
}


VectorDException::VectorDException(const std::string &description, const int &errorId, const ErrorLevel &errorLevel) noexcept	
{
	m_what	= std::string("\nException launched:")
	        + "\n   Level   : " + ErrorLevel2Sstring(errorLevel)
	        + "\n   Code    : " + std::to_string(errorId)
	        + "\n   Message : " + description;
}