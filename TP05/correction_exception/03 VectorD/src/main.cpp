#include <iostream>
#include <stdexcept>

#include "VectorD.hpp"


int main(int argc, char **argv) {

    VectorD<double> v1(3);
    v1[0] = 1.0;
    v1[1] = 0.3; 
    v1[2] = 5.2;

    VectorD<double> v2(4);
    v2[0] = -1.0;
    v2[1] =  3.0; 
    v2[2] = 42.0;
    v2[3] =  2.0;

    try{
        std::cout << "v1 + v2  : " << v1 + v2 << std::endl;
    }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }

    try{
        std::cout << "v1 - v2  : " << v1 - v2 << std::endl;
    }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }

    try{
         std::cout << "v1.v2    : " << v1.dot(v2) << std::endl;
    }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }

    VectorD<double> v3;
    try{
         v3.load("wazoo.vec"); 
    }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }

    try{
        std::cout << "try to save vector "<< std::endl;
        v3.save("/plop/this lesson is incredible.vec");
     }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }
  
    try{
        std::cout << "try to load vector "<< std::endl;
        v3.load("wazoo!!");
     }catch(const std::exception &e){
        std::cerr << e.what() << std::endl;
        std::cerr << "but I can handle it, let's continue" << std::endl << std::endl;
    }

    std::cout << "finished !" << std::endl;
    

    return 0;
}
