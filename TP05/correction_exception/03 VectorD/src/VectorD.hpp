#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <stdexcept>  // special exceptions

#ifndef __VECTORD__HPP
#define __VECTORD__HPP

// g++ -Wall -O2 -std=c++11 main.cpp VectorD.hpp

// Doxygen menu
/// \version 0.1
/// \mainpage
/// \image html myImage.jpg
/// \tableofcontents
/// \section instroduction_sec What for?
/// VectorD is a super tool.
/// \section install_bigsec How to install
/// \subsection dependencies_sec Dependecies
/// \li nothing
/// \li nothing else
/// \li Doxygen (if you want the documentation)
/// \subsection install_sec Install with cmake (Linux)
/// \li go to main dir
/// \li mkdir build
/// \li cd build
/// \li cmake ..
/// \li make
/// \li make html
/// \li The documentation is located in [path to build]/doc/doc-doxygen/html/index.html



/// \class VectorD
/// \brief class defining a vector for linear algebra operations.
template<typename T>
class VectorD {

public:

	/// \brief constructor from a size and default constructor
	/// \param size  : the size of the requested vector (optional)
	/// \param value : the default value used to fill the vector (optional)
    VectorD(const size_t size = 0, const T &value = static_cast<T>(0))
    	: _data(size,value)
    	{};

	/// \brief copy-constructor
	/// \param vec : the source vector to be copied
	VectorD(const VectorD & vec) = default;

    /// \brief destructor
    ~VectorD() = default;

    // copy constructor from other type
#if 1
    template <typename U>
    friend class VectorD;

	/// \brief copy constructor from different template type
	/// \param vec : the source vector to be copied
	template<typename U>
	VectorD<T>(const VectorD<U> &vec) 
		: VectorD<T>(vec.size())
	{
		// access to a private member of VectorD<U>, VectorD<T> and VectorD<U> must be friend
		std::transform(vec._data.begin(), vec._data.end(), _data.begin(), [](const U &x) { return static_cast<T>(x);});
	}
#else
	template<typename U>
	VectorD<T>(const VectorD<U> &vec) 
		: VectorD<T>(vec.size())
	{
		for(size_t i=0; i<vec.size(); ++i)
			_data[i] = static_cast<T>(vec[i]); 
	}
#endif


private :
  
  	// attribute (components of the vector)
    std::vector<T> _data;  /*!< components of the vector */


public :
	

	/// \brief return the size of a Vector
	size_t size() const { return _data.size(); }

    /// \brief compute the inner product between 2 vectors
    /// \param v : the second vector to consider in the dot product.
	/// \throw "std::length_error" exception if the vector sizes are incompatible
    /// \return : the scalar value corresponding to the dot product. 
	T dot(const VectorD &v) const;

	/// \brief compute the norm L2 of a vector
	/// \return the L2 norm of the calling vector	
    T norm() const;

   	/// \brief inplace normalize a vector such its norm is 1.
    void normalize();

    /// \brief save a vector in a file
    /// \param filemane : name of the file (including path) where to save the vector data   
    /// \throw "std::ios_base::failure" exception if the file can not be opened
    void save(const std::string &filename) const;

    /// \brief load a vector from a file, the size of the vector should be already the good one ...
    /// \param filemane : name of the file (including path) to open and load the vector data  
    /// \throw "std::ios_base::failure" exception if the file can not be opened
    void load(const std::string &filename);




    /// \brief affectation operator
	VectorD & operator=(const VectorD &v);

	/// \brief operator to access to the ist element of a vector
	/// \param  i: index of the targeted vector component
	/// \return vector[i] 
	T& operator[](const size_t& i) {return _data[i];};

	/// \brief operator to access to the ist element of a vector (const version)
	/// \param i: index of the targeted vector component
	/// \return vector[i] (constant reference)
	const T& operator[](const size_t& i) const {return _data[i];};

	/// \brief add 2 vectors of same size
	/// \param vector to add to the calling vector (should have the same dimension as the calling vector)
	/// \throw "std::length_error" exception if the vector sizes are incompatible
	/// \return the sum of the current vector and the argument vector
	VectorD operator+(const VectorD &v) const;

	/// \brief substract a vector to *this (where the two vectors have the same size)
	/// \param vector to substract to the calling vector (should have the same dimension as the calling vector)
	/// \throw "std::length_error" exception if the vector sizes are incompatible
	/// \return the sum of the current vector and the argument vector
	VectorD operator-(const VectorD &v) const;

	/// \brief unary minus
	/// \return the minus the calling vector 	
	VectorD operator-() const;

	/// \brief scale a vector with a constant value
	/// \param scale factor
	/// \return the scaled vector
	VectorD operator*(const T &value) const;
};



template<typename T>
VectorD<T> & VectorD<T>::operator=(const VectorD<T> &v){
	if(&v == this) return *this;

	_data = v._data;

	return *this;
}


template<typename T>
VectorD<T> VectorD<T>::operator+(const VectorD<T> &v) const
{
	if(v.size() != this->size())
		throw std::length_error("VectorD::operator+: operand with incompatible size : " + std::to_string(this->_data.size()) + " and " + std::to_string(v._data.size()));

	VectorD<T> result(size());
	std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::plus<double>());

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator-(const VectorD<T> &v) const
{
	if(v.size() != this->size())
		throw std::length_error("VectorD::operator-: operand with incompatible size : " + std::to_string(this->_data.size()) + " and " + std::to_string(v._data.size()));

	VectorD<T> result(size());
	std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::minus<double>());

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator-() const
{
	VectorD<T> result(size());
	for(size_t i=0; i<size(); ++i)
		result[i] = -_data[i];

	return result;
}


template<typename T>
VectorD<T> VectorD<T>::operator*(const T &value) const
{
	VectorD<T> result(*this);
	for(size_t i=0; i<size(); ++i)
		result[i] *= value;

	return result;
}


template<typename T>
T VectorD<T>::dot(const VectorD<T> & v) const {  

	// if(v.size() != this->size())
	// 	throw std::length_error("VectorD::dot: operand with incompatible size : " + std::to_string(this->_data.size()) + " and " + std::to_string(v._data.size()));

    // T result = static_cast<T>(0);
    // for(size_t i=0; i<size(); ++i)
    // 	result += _data[i]*v[i];
    // return result;

   return std::inner_product(_data.begin(), _data.end(), v._data.begin(), static_cast<T>(0));    // handles its own exceptions

}


template<typename T>
T VectorD<T>::norm() const{
	return sqrt(this->dot(*this));
}


template<typename T>
void VectorD<T>::normalize() {
    T vec_norm = this->norm();

    if (std::abs(vec_norm) < static_cast<T>(1.0e-10))
        return;
    
    for (size_t i =0; i<size(); i++) 
        _data[i] /= vec_norm;
}


template<typename T>
std::ostream& operator<< (std::ostream& stream, const VectorD<T>& v){
	if(v.size() == 0){
		stream << "Not initialized yet -> size is 0";
		return stream;
	}
			
	stream << "(";
	for(unsigned int i=0; i<v.size()-1; ++i)
		stream << v[i] << " , ";
 
 	stream << v[v.size()-1] << ")";

	return stream;
}


template<typename T>
void VectorD<T>::save(const std::string &filename) const{

	//open the file
	std::ofstream myfile;
	myfile.open(filename, std::ios::out | std::ios::binary);
	
	if(!myfile.is_open()){
		throw std::ios_base::failure("VectorD::save: error: can not open file: \"" + filename + "\" ");
	}

	// write the vector size
	myfile << size() << std::endl;

	for(size_t i=0; i<size(); ++i)
		myfile << _data[i] << " ";

	myfile.close();
}


template<typename T>
void VectorD<T>::load(const std::string &filename){

	//open the file
	std::ifstream myfile;
	myfile.open(filename, std::ios::in | std::ios::binary);
	if(!myfile.is_open()){
		throw std::ios_base::failure("VectorD::load: error: can not open file: \"" + filename + "\" ");
	}

	// read the vector size
	size_t vectorSize;
	myfile >> vectorSize;
	if(vectorSize != size())
        *this = VectorD(vectorSize);

    // read the data
	for(size_t i=0; i<vectorSize; ++i)
		myfile >> _data[i];

	// close file
	myfile.close();
}




#endif
