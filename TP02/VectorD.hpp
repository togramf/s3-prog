#ifndef  VECTOR_D_TP2
#define  VECTOR_D_TP2
#include <vector>

class VectorD{

public: 
    //Attribut(s)
    std::vector<double> _data;

    //Constructeurs et destructeur
    VectorD(const size_t size=0, const double val=0);
    VectorD(const VectorD &vec);
    ~VectorD()=default;

    //Méthodes
    double dot(const VectorD &v)const;
    double norm()const;

    void display()const;

    //VectorD operator= (const VectorD v)const;

};


#endif