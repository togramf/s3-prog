#include "VectorD.hpp"
#include<iostream>
#include"math.h"

// Constructeurs //
VectorD::VectorD(const size_t size, const double val):_data(size,val){
}

VectorD::VectorD(const VectorD &vec):_data(vec._data){
}

// Méthodes //
void VectorD::display() const{
    for(size_t i=0; i<_data.size();i++)
        std::cout << _data[i] << " ";
    
    std::cout << std::endl;
}

double VectorD::dot(const VectorD &v) const {
    double produit=0;

    if(v._data.size()==_data.size()){
        for(size_t i=0; i<_data.size(); i++)
            produit+=_data[i]*v._data[i];
    } else
        std::cout << "ALED les vecteurs n'ont pas la même taille" << std::endl;
    
    return produit;
}

double VectorD::norm()const {
    double norme=0;
    
    for(size_t i=0; i<_data.size();i++){
        norme+=_data[i]*_data[i];
        norme=sqrt(norme);
    }
    return norme;
}



