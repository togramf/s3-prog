// ================================
// POO C++ - IMAC 2
// TP 2 - Exercices
// ================================

#include "VectorD.hpp"
#include<iostream>

int main(){

    //création et remplissage de deux vecteurs
    VectorD vec1(3);    vec1._data[1] = 5;
    VectorD vec2(3);    vec2._data[1] = 1;
    
    //test des fonctions display, dot et norm 
    std::cout <<"vecteur1 : ";    vec1.display();
    std::cout <<"vecteur2 : ";    vec2.display();

    std::cout << "produit scalaire de vecteur1 et vecteur2 : "<< vec1.dot(vec2) << std::endl;
    std::cout << "norme de vecteur1 : "<< vec1.norm() << std::endl;

    // étude de l'opérateur '='
    std::cout<<std::endl<<"test de l'opérateur '=' : "<<std::endl;
    VectorD vec3;
    vec3=vec1;
    std::cout <<"vecteur3 '=' vecteur1 : ";    vec3.display();
    
    vec1._data[2]=65;
    std::cout <<"Si on change vecteur1 : ";  vec1.display(); 
    std::cout <<"vecteur3 : "; vec3.display(); 
    

    return 0;
}

/** NOTICE 
 * compilation :  g++ -Wall -O2 -std=c++11 main.cpp VectorD.cpp -o tp2
 * execution : ./tp2 
 */