#include <iostream>

#include "VectorD.hpp"

int main(int argc, char **argv) {

    VectorD v(3);
    v[0] = 1.0;
    v[1] = 0.3; 
    v[2] = 5.2;

    std::cout << "v             : " << v << std::endl;
    std::cout << "size of v     : " << v.size() << std::endl;
    std::cout << "v[2]          : " << v[2] << std::endl << std::endl;
    std::cout << "v*2           : " << v*2 << std::endl;
    std::cout << "2*v           : " << 2*v << std::endl;
    std::cout << "-v            : " << -v << std::endl;
    std::cout << "norm(v)       : " << v.norm() << std::endl;
    v.normalize();
    std::cout << "v2 normalized : " << v << std::endl;

    return 0;
}
