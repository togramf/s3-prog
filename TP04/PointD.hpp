#ifndef  POINT_D_TP2
#define  POINT_D_TP2
#include <vector>

template <typename Type>
class PointD {

    private : 
        std::vector<Type> _data;
        
    public: 
        //constructeurs 
        PointD(const size_t size, const Type val=0):_data(size,val){}

        template <typename U> 
        PointD<Type>(const PointD<U> &vec) : PointD(vec._data.size()){
            for(size_t i = 0; i<_data.size(); i++)
                _data[i] = (Type)vec._data[i]; 
        }

        //destructeurs 
        ~PointD()=default;

        //opérateurs 
        
        void display() const{
            for(size_t i=0; i<_data.size();i++){
                std::cout << _data[i] << " ";

            }
            std::cout << std::endl;
        }

};


#endif