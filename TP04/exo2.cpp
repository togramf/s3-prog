// ================================
// POO C++ - IMAC 2
// TP 4 - Exercice 2
// ================================

#include <iostream>
#include "VectorD.hpp"


int main(){
    VectorD<int> vecInt(3, 2);
    vecInt.display();

    VectorD<float> vecFloat (5, 0.2);
    vecFloat.display();

    VectorD<char> vecChar(6, 'm');
    vecChar.display();

    VectorD<float> A(3, 4.2);
    A.display();
    VectorD<int> B(A);
    B.display();

    return 0;
}

// NOTICE 
// g++ -Wall -O2 -std=c++11 exo2.cpp -o exo2.out 
// ./exo2.out