// ================================
// POO C++ - IMAC 2
// TP 4 - Exercice 1
// ================================

#include <iostream>

template <typename T>
T getMinimum (const T&a, const T&b){
    return a<b ? a : b;
}

template <>
char getMinimum <char>(const char &a, const char &b){
    return std::tolower(a)<std::tolower(b) ? a : b ;
}

int main(){
    //Test de getMinimum
    std::cout << getMinimum(42, 54) << std::endl;
    std::cout << getMinimum(4.2, 5.4) << std::endl;
    std::cout << getMinimum('v', 'n') << std::endl;

    //test avec 2 types différents
    std::cout << getMinimum<float>(4, 5.4) << std::endl;

    //test avec 'a' et 'Z'
    std::cout << getMinimum('a', 'Z') << std::endl;


    return 0;
}

// NOTICE 
// g++ -Wall -O2 -std=c++11 exo1.cpp  -o exo1.out 
// ./exo1.out