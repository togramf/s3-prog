#include <iostream>
#include <string>
#include <vector>
#include <algorithm>  // transform, inner
#include <cmath>      // sqrt
#include <cassert>    // (bad way) to handle user errors
#include <fstream>


#ifndef __VECTORD__HPP
#define __VECTORD__HPP

template <typename T>

/// \class VectorD
/// \brief class defining a vector for linear algebra operations.
class VectorD {
    
    private :
    
        std::vector<T> _data;     /*!< components of the vector */

    public:

        /// \brief constructor from a size and default constructor
        /// \param size : the size of the requested vector (optional)
        VectorD(const size_t size = 0) : _data(size) {}

        /// \brief constructor from a size that fills the vector with a constant
        /// \param size : the size of the requested vector
        /// \param constant : the constant value used to fill the vector
        VectorD(const size_t size, const T &value) : VectorD(size)
        {
            std::fill(_data.begin(), _data.end(), value);
        }

        template <typename U>
        friend class VectorD; // si on veut accéder à des attr privés 

        /// \brief copy-constructor
        /// \param v : the source vector to be copied
        template <typename U>
        VectorD<T>(const VectorD<U> &v): VectorD<T>(v.size())
        {
            for(size_t i = 0; i<_data.size(); i++)
                _data[i] = (T)v[i];
        }

        /// \brief destructor
        ~VectorD() = default;

        
        /// \brief return the size of a Vector
        inline size_t size() const {
            return _data.size();
        }

        /// \brief affectation operator
        VectorD & operator=(const VectorD &v)
        {
            if(&v == this) return *this;

            _data = v._data;
            return *this;
        }

        /// \brief operator to access to the ist element of a vector
        /// \param  i: index of the targeted vector component
        /// \return vector[i] 
        T& operator[](const size_t& i)
        {
            return _data[i];
        }

        /// \brief operator to access to the ist element of a vector (const version)
        /// \param i: index of the targeted vector component
        /// \return vector[i] (constant reference)
        const T& operator[](const size_t& i) const
        {
            return _data[i];
        }

        /// \brief add 2 vectors of same size
        /// \param vector to add to the calling vector (should have the same dimension as the calling vector)
        /// \return the sum of the current vector and the argument vector
        VectorD operator+(const VectorD &v) const
        {
            assert(v.size() == this->size() && "error: VectorD::operator+: operands with incompatible size");
            
            VectorD result(size());
            std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::plus<T>());

            return result;
        }

        /// \brief substract a vector to *this (where the two vectors have the same size)
        /// \param vector to substract to the calling vector (should have the same dimension as the calling vector)
        /// \return the sum of the current vector and the argument vector
        VectorD operator-(const VectorD &v) const
        {
            assert(v.size() == this->size() && "error: VectorD::operator-: operands with incompatible size");
        
            VectorD result(size());
            std::transform(_data.begin(), _data.begin()+size(), v._data.begin(), result._data.begin(), std::minus<T>());

            return result;
        }


        /// \brief unary minus
        /// \return the minus the calling vector 
        VectorD operator-() const
        {
            VectorD<T> result(size());
            for(size_t i=0; i<size(); ++i)
                result[i] = -_data[i];

            return result;
        }

        /// \brief scale a vector with a constant value
        /// \param scale factor
        /// \return the scaled vector
        VectorD<T> operator*(const U &value) const
        {
            VectorD<T> result(*this);
            for(size_t i=0; i<size(); ++i)
                result[i] *= value;

            return result;
        }

        /// \brief compute the norm L2 of a vector
        /// \return the L2 norm of the calling vector
        double norm() const
        {
            return sqrt(this->dot(*this));
        }

        /// \brief inplace normalize a vector such its norm is 1.
        void normalize()
        {
            double vec_norm = this->norm();

            if (std::abs(vec_norm) < 1.0e-10)
                return;
            
            for (size_t i =0; i<size(); i++) 
                _data[i] /= vec_norm;
        }

        /// \brief compute the inner product between 2 vectors
        /// \param v : the second vector to consider in the dot product.
        /// \return : the scalar value corresponding to the dot product. 
        double dot(const VectorD &v) const{  

            assert(v.size() == this->size() && "error: VectorD::dot: operands with incompatible size");

            double result = 0.0;
            for(size_t i=0; i<size(); ++i)
                result += _data[i]*v[i];

            // return std::inner_product(_data.begin(), _data+size(), v._data.begin(),0.0);

            return result;
        }


        /// \brief save a vector in a file
        /// \param filemane : name of the file (including path) where to save the vector data
        /// \return EXIT_SUCCESS if the file is save correctly, else EXIT_FAILURE 
        //int save(const std::string &filename) const;

        /// \brief load a vector from a file, the size of the vector should be already the good one ...
        /// \param filemane : name of the file (including path) to open and load the vector data
        /// \return EXIT_SUCCESS if the file is save correctly, else EXIT_FAILURE 
        //int load(const std::string &filename);

        /// \brief display the elements of the vector
        void display() const{
            for(size_t i=0; i< _data.size(); ++i)
                std::cout << _data[i] << " ";
            std::cout << std::endl;
        }

};


	/// \brief scale a vector with a constant value
	/// \param scale factor
	/// \param vec is the vector to be scaled
	/// \return the scaled vector
	VectorD<T> operator*(const double value, const VectorD<T> &vec){
        return vec * value;
    }


	/// \brief overload the operator << for VectorD
    /// \param stream : input stream
    /// \param v : the vector to output
    /// \return the output stream containing the vector data
    std::ostream& operator<< (std::ostream& stream, const VectorD& v){
        if(v.size() == 0){
            stream << "Not initialized yet -> size is 0";
            return stream;
        }
                
        stream << "(";
        for(unsigned int i=0; i<v.size()-1; ++i)
            stream << v[i] << " , ";
    
        stream << v[v.size()-1] << ")";

        return stream;
    }

#endif
