#include <random>
#include <vector>
#include <string>
#include <algorithm>

#include <gtest/gtest.h>

#include "VectorD.hpp"


/////////////////////////////////////////////////////
// constructors

TEST (VectorDConstructor, defaultConstructor) { 
	VectorD vec;
	ASSERT_EQ (vec.size(), 0);
}

TEST (VectorDConstructor, recopyConstructor) {
	
	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// generate random vector
		VectorD vec1(dim);
		double scal = gen();
		for(size_t i=0; i<dim; i++)
			vec1[i]=gen();
			
		// calculate result
		VectorD vec2(vec1);

		for(size_t i=0; i<dim; i++)
			ASSERT_DOUBLE_EQ(vec1[i], vec2[i]);

	}

}

/////////////////////////////////////////////////////
// arithmetic

TEST (VectorDArithmetic, plus) {

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// generate random data
		std::vector<double> data1(dim), data2(dim);
		std::generate(data1.begin(), data1.end(), gen);
		std::generate(data2.begin(), data2.end(), gen);

		// build the corresponding VectorD
		VectorD vec1(dim), vec2(dim), vec3(dim);
		for(size_t i=0; i<dim; ++i){
			vec1[i] = data1[i];
			vec2[i] = data2[i];
		}

		vec3 = vec1 + vec2;
		ASSERT_EQ (vec3.size(), dim);

		for(size_t i=0; i<dim; ++i){
	    	ASSERT_DOUBLE_EQ (vec3[i], data1[i] + data2[i]);    // EXPECT_DOUBLE_EQ would be fine too
		}
	}
}

TEST (VectorDArithmetic, minus) {

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// generate random data
		std::vector<double> data1(dim), data2(dim);
		std::generate(data1.begin(), data1.end(), gen);
		std::generate(data2.begin(), data2.end(), gen);

		// build the corresponding VectorD
		VectorD vec1(dim), vec2(dim), vec3(dim);
		for(size_t i=0; i<dim; ++i){
			vec1[i] = data1[i];
			vec2[i] = data2[i];
		}

		vec3 = vec1 - vec2;
		ASSERT_EQ (vec3.size(), dim);

		for(size_t i=0; i<dim; ++i){
	    	ASSERT_DOUBLE_EQ (vec3[i], data1[i] - data2[i]);  // EXPECT_DOUBLE_EQ would be fine too
		}
	}
}

TEST (VectorDArithmetic, dot_alea) {
	
	// vérification sur des vecteurs aléatoires

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// build the corresponding VectorD
		VectorD vec1(dim), vec2(dim);
		double expected_result = 0.0;

		for(size_t i=0; i<dim; ++i){
			vec1[i] = gen();
			vec2[i] = gen();
			expected_result += vec1[i]*vec2[i];
		}

		double result1 = vec1.dot(vec2);
		double result2 = vec2.dot(vec1);

		// test 
	    ASSERT_DOUBLE_EQ (result1, expected_result);    // EXPECT_DOUBLE_EQ would be fine too
		
	}
} 

TEST (VectorDArithmetic, dot_commutativity){

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// build the corresponding VectorD
		VectorD vec1(dim), vec2(dim);

		for(size_t i=0; i<dim; ++i){
			vec1[i] = gen();
			vec2[i] = gen();
		}
		
		// test commutativité
		ASSERT_DOUBLE_EQ (vec1.dot(vec2), vec2.dot(vec1));
	}
}

TEST (VectorDArithmetic, dot_positiveDefined){

	// vérification des propriétés du PS

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// build the corresponding VectorD
		VectorD vec1(dim);

		for(size_t i=0; i<dim; ++i){
			vec1[i] = gen();
		}

		// test positivité
		EXPECT_GE(vec1.dot(vec1), 0.0);
	}
}

TEST (VectorDArithmetic, dot_cosinus_law){

	// vérification de la loi des cosinus

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// build the corresponding VectorD
		VectorD vec1(dim), vec2(dim), vec_diff(dim);

		for(size_t i=0; i<dim; ++i){
			vec1[i] = gen();
			vec2[i] = gen();
			vec_diff[i] = vec1[i]-vec2[i];
		}

		double result1 = vec1.dot(vec2);

		// test loi des cosinus
		double result_cos = 0.5 * (vec1.dot(vec1) + vec2.dot(vec2) - (vec_diff).dot(vec_diff));
		// ASSERT_NEAR (result1, result_cos, (double)1/100000);
		ASSERT_NEAR (result1, result_cos, std::numeric_limits<double>::round_error());
	}
}

TEST (VectorDArithmetic, scalar_multiplication) {

	const size_t maxSize = 1000;  // max size of the tested vectors
	std::mt19937 generator(0);
	std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);
	std::uniform_real_distribution<double> uniformDistributionValue(-int(maxSize),maxSize);
	auto gen = [&uniformDistributionValue, &generator](){ return uniformDistributionValue(generator);};

	// run many times the same test with different values
	for(int run=0; run<100; ++run){

		// define a vector dimension
		const size_t dim = uniformIntDistribution(generator);

		// generate random vector
		VectorD vec(dim), result(dim);
		double scal = gen();
		for(size_t i=0; i<dim; i++)
			vec[i]=gen();
			
		// calculate result
		result = scal*vec;

		for(size_t i=0; i<dim; i++)
			ASSERT_DOUBLE_EQ(result[i], vec[i]*scal);

	}
}


/////////////////////////////////////////////////////
// exception

TEST (VectorDException, loadExceptionMessage) {
	const std::string filename = "filename.txt";
	const std::string expectedException = "VectorD::load: error: can not open file: " + filename;

	// check the exception message
    try{
		VectorD vec;
        vec.load(filename);
    }
    catch( const std::exception &e){
        EXPECT_TRUE( std::string(e.what()).find(expectedException) == 0);
    }
}


TEST (VectorDException, loadExceptionType) {

	// check exception type
	VectorD vec;
	EXPECT_THROW(vec.load("filename.txt"), std::ios_base::failure);
}

TEST (VectorDException, plusExceptionType) {

	// check exception type
	VectorD vec1(2), vec2(5);
	EXPECT_THROW(vec1+vec2, std::length_error);
}

TEST (VectorDException, plusExceptionMessage) {

	// check the exception message
	VectorD vec1(2), vec2(5);
	const std::string expectedException = "VectorD::operator+: operand with incompatible size : " + std::to_string(2) + " and " + std::to_string(5);

	// check the exception message
    try{
		VectorD vec3 = vec1 + vec2;
    }
    catch( const std::exception &e){
        EXPECT_TRUE( std::string(e.what()).find(expectedException) == 0);
    }
}

TEST (VectorDException, plusExceptionMessageCorrection) {

    const std::string expectedException = "VectorD::operator+: operand with incompatible size";
    const size_t maxSize = 1000;  // max size of the tested vectors
    std::mt19937 generator(0);
    std::uniform_int_distribution<int> uniformIntDistribution(1,maxSize);

    // run many times the same test with different values
    for(int run=0; run<100; ++run){

        // define vector dimensions
        const size_t dim1 = uniformIntDistribution(generator);
        const size_t dim2 = uniformIntDistribution(generator);

        // ignore if same size
        if(dim1 == dim2)
            continue;

        VectorD v1(dim1);
        VectorD v2(dim2);

        // check the exception message
        try{
            v1+v2;
        }
        catch( const std::exception &e){
            EXPECT_TRUE( std::string(e.what()).find(expectedException) == 0);
        }

        // check exception type
        EXPECT_THROW(v1+v2, std::length_error);
    }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

